function rifFocusChange(value, objCampo, limit, clean) {

    if(value.length === limit) {
        objCampo.focus();
        
        if(clean === true) {
            if(objCampo.value.length > 0) 
                objCampo.value = '';
        }
    }
}