<?php
use \FDSoil\DbFunc as DbFunc;

trait CommonColor
{
    
    function colorList() { return DbFunc::exeQryFile(self::_path()."color_select.sql", null, false, '', 'common'); }
    
    function colorAuxList() { return DbFunc::exeQryFile(self::_path()."color_aux_select.sql", $_POST, false, '', 'common'); }

}

