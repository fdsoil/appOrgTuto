SELECT id,  CASE 
		WHEN codprocedencia = 0 THEN 
			seccion 
		ELSE 
			CASE WHEN SUBSTRING(arancel,3,8)='00000000' THEN 
				SUBSTRING(arancel,1,2) 
			ELSE 
				CASE WHEN SUBSTRING(arancel,5,6)='000000' THEN 
					SUBSTRING(arancel,1,4) 
				ELSE 
					CASE WHEN SUBSTRING(arancel,7,4)='0000' THEN 
						SUBSTRING(arancel,1,6) 
					ELSE 
						CASE WHEN SUBSTRING(arancel,9,4)='00' THEN 
							SUBSTRING(arancel,1,8) 
						ELSE 
							arancel 
						END  
					END 
				END
			END
		END,
CASE WHEN codprocedencia = 0 THEN '<b><i>SECCIÓN</i></b> '||replace(descripcion, 'SECCIÓN', '')
ELSE
       descripcion END, titulo, codprocedencia --codigo_completo, codigo_completo, capitulo, sub_capitulo,
  FROM aranceles.aranceles
WHERE codprocedencia = {fld:codprocedencia}
ORDER BY 2;
