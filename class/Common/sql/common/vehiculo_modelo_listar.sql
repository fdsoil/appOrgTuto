SELECT 
mo.id, 
t.descripcion as tipo,
ma.descripcion as marca,
mo.descripcion, 
mo.id_tipo,
mo.id_marca
FROM vehiculo.modelo mo
INNER JOIN vehiculo.tipo t ON mo.id_tipo = t.id
INNER JOIN vehiculo.marca ma ON mo.id_marca = ma.id