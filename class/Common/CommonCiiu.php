<?php
use \FDSoil\DbFunc as DbFunc;

trait CommonCiiu
{
    
    function ciiuSeccionList() { return DbFunc::exeQryFile(self::_path()."ciiu_seccion_list_select.sql", null, false, '', 'common'); }

    function ciiuDivisionList() { return DbFunc::exeQryFile(self::_path()."ciiu_division_list_select.sql", $_POST, false, '', 'common'); }

    function ciiuGrupoList() { return DbFunc::exeQryFile(self::_path()."ciiu_grupo_list_select.sql", $_POST, false, '', 'common'); }

    function ciiuClaseList() { return DbFunc::exeQryFile(self::_path()."ciiu_clase_list_select.sql", $_POST, false, '', 'common'); }

}
